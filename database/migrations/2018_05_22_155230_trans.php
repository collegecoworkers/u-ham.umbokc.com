<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Trans extends Migration
{

	public function up()
	{
		Schema::create('trans', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('sum');
			$table->integer('from_card');
			$table->integer('to_card');
			$table->string('comment');

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	public function down()
	{
		//
	}
}
