<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cards extends Migration
{

	public function up()
	{
		Schema::create('cards', function (Blueprint $table) {
			$table->increments('id');

			$table->integer('number');
			$table->integer('cvv');

			$table->integer('sum');
			$table->integer('bank_id');
			$table->integer('user_id');

			$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
		});
	}

	public function down()
	{
		//
	}
}
