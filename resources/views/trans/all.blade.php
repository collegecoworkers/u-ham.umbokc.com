@extends('layout.app')
@section('content')

<div class="row">
	<h2>Список транзакции</h2>
	<div class="col-md-4" ea-j='ml=-10px'>
		<a href="/trans" class="btn btn-primary"> Совершить транзакцию</a>
	</div>
	<table class="table table-index">
		<thead>
			<tr>
				<th>Карта отправителя</th>
				<th>Карта получателя</th>
				<th>Сумма</th>
				<th>Комментарий</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($transs as $item)
			<tr>
				<td>{{ $item->getCardF()->number }}</td>
				<td>{{ $item->getCardT()->number }}</td>
				<td>
					@if (in_array($item->getCardT()->number, $my_cards_num))
						<i class="fa fa-plus" ea-j='c=#f58c9e'></i>
					@else
						<i class="fa fa-minus" ea-j='c=#00a6ff'></i>
					@endif
					{{ $item->sum }} руб.
				</td>
				<td>{{ $item->comment }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
