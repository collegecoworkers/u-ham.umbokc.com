@extends('layout.app')
@section('content')

<div class="row">
	<h2>Список транзакции</h2>
	<table class="table table-index">
		<thead>
			<tr>
				<th>Карта отправителя</th>
				<th>Карта получателя</th>
				<th>Сумма</th>
				<th>Комментарий</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($transs as $item)
			<tr>
				<td>{{ $item->getCardF()->number }}</td>
				<td>{{ $item->getCardT()->number }}</td>
				<td>
					{{ $item->sum }} руб.
				</td>
				<td>{{ $item->comment }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
