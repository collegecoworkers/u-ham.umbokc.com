@extends('layout.app')
@section('content')


<div class="row">
	<div class="col-lg-8 col-lg-offset-2">
		<h2>Новая транзакция</h2>
		{!! Form::open(['url' => '/transsave' ]) !!}

    <div class="form-group">
      <label class="control-label">Списание с карты</label>
      {!! Form::select('from_card', $cards, null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      <label class="control-label">Картра получателя</label>
      {!! Form::number('to_card', '', ['class' => 'form-control', 'required' => '']) !!}
    </div>

    <div class="form-group">
      <label class="control-label">Сумма</label>
      {!! Form::number('sum', '', ['class' => 'form-control', 'required' => '']) !!}
    </div>

    <div class="form-group">
      <label class="control-label">Комментарий</label>
      {!! Form::text('comment', '', ['class' => 'form-control', 'required' => '']) !!}
    </div>

    <div class="form-actions">
      <button type="submit" class="btn btn-success">Отправить</button>
    </div>
    {!! Form::close() !!}

  </div>
</div>

@endsection
