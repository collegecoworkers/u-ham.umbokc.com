@php
use App\User;
@endphp
<!DOCTYPE html>
<html lang="en" ea>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/assets/css/animate.min.css" rel="stylesheet"/>
	<link href="/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
	<link href="/assets/css/demo.css" rel="stylesheet" />

	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
	<link href="/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=2">

	<link href="/assets/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css" rel="stylesheet">

	<script src="http://cdn.umbokc.com/ea/src/ea.js?v=2"></script>
</head>
<body>

	<div class="wrapper">
		<div class="main-panel">
			<nav class="navbar navbar-default navbar-fixed">
				<div class="container-fluid">    
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/">{{ config('app.name', 'Laravel') }}</a>
					</div>
					<div class="collapse navbar-collapse">       
						<ul class="nav navbar-nav navbar-left">
						</ul>

						<ul class="nav navbar-nav navbar-right">
							<li><a href="/" >Главная</a></li>
							<li><a href="/transs" >Мои транзакции</a></li>
							@if (User::isAdmin())
								<li><a href="/admin/users" >Пользователи</a></li>
								<li><a href="/admin/banks" >Банки</a></li>
								<li><a href="/admin/trans" >Транзакции</a></li>
							@endif
							<li>
								<form id="logout-form" action="/logout" method="POST" style="display: none;">{{ csrf_field() }}</form>
								<a class="logout" href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<div class="content">
				<div class="container-fluid">    
					@yield('content')
				</div>    
			</div>

			<footer class="footer">
				<div class="container-fluid">
					<p class="copyright pull-right">
						&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.
					</p>
				</div>
			</footer>

		</div>   
	</div>

	<script src="/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/assets/js/bootstrap-notify.js"></script>
	<script src="/assets/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.js"></script>
	<script src="/assets/js/light-bootstrap-dashboard.js"></script>
	<script src="/assets/js/demo.js"></script>

	<script>
		$(function () {
			$('#mycp').colorpicker();
		});

	</script>

</body>
</html>
