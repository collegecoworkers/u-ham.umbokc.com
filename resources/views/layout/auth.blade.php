<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	
	<link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/assets/css/animate.min.css" rel="stylesheet"/>
	<link href="/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
	<link href="/assets/css/demo.css" rel="stylesheet" />

	<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
	<link href="/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>
<body>


	<div class="content">
		<div class="container-fluid">
			<div class="row">			
				@yield('content')
			</div>
		</div>
	</div>


	<footer class="footer">
		<div class="container-fluid">
			<p class="copyright pull-right">
				&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.
			</p>
		</div>
	</footer>

	<script src="/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/assets/js/light-bootstrap-dashboard.js"></script>
	<script src="/assets/js/demo.js"></script>
</body>
</html>
