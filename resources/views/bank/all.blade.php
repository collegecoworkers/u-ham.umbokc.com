@extends('layout.app')
@section('content')

<div class="row">
	<h2>Список банков</h2>
	<div class="col-md-4" ea-j='ml=-10px'>
		<a href="/bank/add" class="btn btn-primary">Добавить банк</a>
	</div>
	<table class="table table-index">
		<thead>
			<tr>
				<th>Название</th>
				<th>Описание</th>
				<th>Цвет</th>
				<th class="col-md-2">Действия</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($banks as $item)
			<tr>
				<td>{{ $item->title }}</td>
				<td>{{ $item->desc }}</td>
				<td>{{ $item->color }}</td>
				<td>
					<a href="/bank/edit/{{$item->id}}"><i class="fa fa-edit"></i></a>
					<a href="/bank/delete/{{$item->id}}" onclick="return confirm('Вы уверенны?')"><i class="fa fa-trash"></i></a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
