{!! Form::open(['url' => isset($model) ? '/bank/update/' . $model->id : '/bank/create/' ]) !!}

	<div class="form-group">
		<label class="control-label">Название</label>
		{!! Form::text('title', isset($model) ? $model->title : '', ['class' => 'form-control', 'required' => '']) !!}
	</div>

	<div class="form-group">
		<label class="control-label">Описание</label>
		{!! Form::textarea('desc', isset($model) ? $model->desc : '', ['class' => 'form-control', 'required' => '']) !!}
	</div>

	<div class="form-group">
		<label class="control-label">Цвет</label>
		{!! Form::text('color', isset($model) ? $model->color : '#5367ce', ['id' => 'mycp','class' => 'form-control', 'required' => '']) !!}
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Отправить</button>
	</div>
{!! Form::close() !!}

