@extends('layout.app')
@section('content')

<div class="row">
	<div class="col-lg-8 col-lg-offset-2">
		<h2>Изменить банк</h2>
		<br>
		<br>
		@include('bank._form')
	</div>
</div>
@endsection
