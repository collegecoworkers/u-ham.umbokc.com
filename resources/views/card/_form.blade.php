{!! Form::open(['url' => '/card/create/' ]) !!}

	<div class="form-group">
		<label class="control-label">Выберите банк</label>
		{!! Form::select('bank_id', $banks, null, ['class' => 'form-control']) !!}
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Отправить</button>
	</div>
{!! Form::close() !!}

