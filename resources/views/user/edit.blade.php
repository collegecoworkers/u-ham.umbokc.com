@extends('layout.app')
@section('content')

<div class="row">
  <div class="col-lg-8 col-lg-offset-2">
    <h2>Изменить пользователя</h2>
    <br>
    <br>
    @include('user._form')
  </div>
</div>
@endsection
