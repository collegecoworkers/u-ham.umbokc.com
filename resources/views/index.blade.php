@extends('layout.app')
@section('content')

<div class="row">
	<div class="col-md-12" ea-s='m:b:big'>
		<a href="/card/add" class="btn btn-primary">+ Добавить карту</a>
	</div>
	@foreach ($cards as $item)
		<div class="col-md-4">
			<div class="card" ea-j='bgc='>
				<div class="header">
					<h4 class="title">{{ $item->getNumber() }}</h4>
					<p class="category">Баланс: <b>{{ $item->sum }} руб.</b></p>
				</div>
				<div class="content">
					<div class="footer">
						<div class="legend">
							<i class="fa fa-circle" ea-j='c={{ $item->getBank()->color }}'></i> {{ $item->getBank()->title }}
						</div>
						<hr>
						<div class="stats">
							<i class="pe-7s-lock"></i> Cvv: {{ $item->cvv }}
						</div>                                    
					</div>
				</div>
			</div>
		</div>
	@endforeach
</div>

@endsection

