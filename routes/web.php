<?php

Auth::routes();

Route::get('/', 'SiteController@Index');

// Транзакции
Route::get('/transs', 'SiteController@Transs');
Route::get('/trans', 'SiteController@Trans');
Route::post('/transsave', 'SiteController@TransSave');

Route::get('/card/add', 'CardController@Add');
Route::get('/card/edit/{id}', 'CardController@Edit');
Route::get('/card/delete/{id}', 'CardController@Delete');
Route::post('/card/create', 'CardController@Create');
Route::post('/card/update/{id}', 'CardController@Update');
Route::post('/card/card/{id}', 'CardController@Card');

// admin
Route::get('/admin/banks', 'BankController@All');
Route::get('/bank/add', 'BankController@Add');
Route::get('/bank/edit/{id}', 'BankController@Edit');
Route::get('/bank/delete/{id}', 'BankController@Delete');
Route::post('/bank/create', 'BankController@Create');
Route::post('/bank/update/{id}', 'BankController@Update');

Route::get('/admin/trans', 'AdminController@Trans');

Route::get('/admin/users', 'UserController@Index');
Route::get('/user/add', 'UserController@Add');
Route::get('/user/edit/{id}', 'UserController@Edit');
Route::get('/user/delete/{id}', 'UserController@Delete');
Route::post('/user/create', 'UserController@Create');
Route::post('/user/update/{id}', 'UserController@Update');
