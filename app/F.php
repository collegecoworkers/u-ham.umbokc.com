<?php
namespace App;

class F {

	static function toArr($items, $val = 'title', $key = 'id'){
		$items_arr = [];
		foreach ($items as $item){
			if(is_callable($val)){
				$items_arr[$item->{$key}] = $val($item);
			} else {
				$items_arr[$item->{$key}] = $item->{$val};
			}
		}
		return $items_arr;
	}

	static function getField($items, $filed = 'id'){
		$items_fields = [];
		foreach ($items as $item){
			$items_fields[] = is_object($item) ? $item->{$filed} : $item[$filed];
		}
		return $items_fields;
	}
}
