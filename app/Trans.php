<?php
namespace App;

class Trans extends MyModel{

  function getCardF() {
    return Card::getById($this->from_card);
  }

  static function getsMy() {
    $cards = Card::getsMy();
    $ids = F::getField($cards, 'id');
    $res = [];
    foreach (self::whereIn('from_card', $ids)->get() as $item) $res[] = $item;
    foreach (self::whereIn('to_card', $ids)->get() as $item) $res[] = $item;
    return $res;
  }

  function getCardT() {
    return Card::getById($this->to_card);
  }

}
