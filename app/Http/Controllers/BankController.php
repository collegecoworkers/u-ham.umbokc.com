<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Bank
};

class BankController extends Controller {

	function __construct(){
	}

	function All() {
		$banks = Bank::all();
		return view('bank.all')->with([
			'banks' => $banks,
		]);
	}
	function Add() {
		return view('bank.add');
	}
	function Edit($id) {
		$bank = Bank::getById($id);
		return view('bank.edit')->with([
			'model' => $bank,
		]);
	}
	function Create(Request $request) {
		$model = new Bank();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->color = request()->color;

		$model->save();
		return redirect('/admin/banks');
	}
	function Update($id, Request $request) {
		$model = Bank::getById($id);

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->color = request()->color;

		$model->save();
		return redirect('/admin/banks');
	}
	function Delete($id) {
		Bank::where('id', $id)->delete();
		return redirect()->back();
	}
}
