<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Cat,
	Post,
	User
};

class CatController extends Controller {

	function __construct(){
	}

	function All() {
		$cats = Cat::all();
		return view('cat.all')->with([
			'cats' => $cats,
		]);
	}
	function Add() {
		return view('cat.add');
	}
	function Edit($id) {
		$cat = Cat::getById($id);
		return view('cat.edit')->with([
			'model' => $cat,
		]);
	}
	function Create(Request $request) {
		$model = new Cat();

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect('/admin/cats');
	}
	function Update($id, Request $request) {
		$model = Cat::getById($id);

		$model->title = request()->title;
		$model->desc = request()->desc;

		$model->save();
		return redirect('/admin/cats');
	}
	function Delete($id) {
		Cat::where('id', $id)->delete();
		return redirect()->back();
	}
}
