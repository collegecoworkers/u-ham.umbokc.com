<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Trans
};

class AdminController extends Controller {

	function __construct(){
    $this->middleware('auth');
	}

	function Trans() {
		return view('trans._all')->with([
			'transs' => Trans::all(),
		]);
	}
}
