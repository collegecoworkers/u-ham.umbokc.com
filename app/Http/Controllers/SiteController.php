<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Bank,
	Card,
	Trans,
	User
};

class SiteController extends Controller
{

	function __construct(){
    $this->middleware('auth');
	}

	function Index() {
		$cards = Card::getsMy();
		return view('index')->with([
			'cards' => $cards,
		]);
	}

	function Transs() {
		return view('trans.all')->with([
			'transs' => Trans::getsMy(),
			'my_cards_num' => F::toArr(Card::getsMy(), 'number'),
		]);
	}

	function Trans() {
		return view('trans.add')->with([
			'cards' => F::toArr(Card::getsMy(), 'number'),
		]);
	}

	function TransSave(Request $request) {
		$model = new Trans();

		$fc = Card::getById(request()->from_card);
		$tc = Card::getBy('number', request()->to_card);

		$model->from_card = $fc->id;
		$model->to_card = $tc->id;
		$model->sum = request()->sum;

		if ($fc->sum < $model->sum) return redirect('/transs');

		$fc->sum -= intval($model->sum);
		$tc->sum += intval($model->sum);

		$fc->save();
		$tc->save();

		$model->comment = request()->comment;

		$model->save();

		return redirect('/transs');
	}

}
