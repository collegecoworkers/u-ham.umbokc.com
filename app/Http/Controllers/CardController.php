<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Bank,
	Card,
	Trans,
	User
};

class CardController extends Controller {

	function __construct(){
		$this->middleware('auth');
	}

	function Add() {
		return view('card.add')->with([
			'banks' => Bank::allArr(),
		]);
	}
	function Create(Request $request) {
		$model = new Card();

		$model->number = Card::genNumber();
		$model->cvv = Card::genCvv();
		$model->sum = 50000;
		$model->bank_id = request()->bank_id;
		$model->user_id = User::id();

		$model->save();
		return redirect('/');
	}
	function Delete($id) {
		Card::where('id', $id)->delete();
		return redirect()->back();
	}
}
