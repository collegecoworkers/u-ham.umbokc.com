<?php
namespace App;

class Card extends MyModel{

	function getNumber() {
		$n = (string) $this->number;
		$n = substr_replace($n, ' ', 3, 0);
		return $n;
	}

	function getBank() {
		return Bank::getById($this->bank_id);
	}

	static function getsMy() {
		return self::getsBy('user_id', User::id());
	}

	static function genNumber() {
		$n = mt_rand(100000, 999999);

		if (self::getCount('number', $n) > 0) {
			return self::genNumber();
		}
		return $n;
	}

	static function genCvv() {
		return mt_rand(100, 999);
	}

}
